# Workflow Activities for LibreFoodPantry Onboarding

**NOTE: These activities are under active development. There is currently a single activity that has been used once in Spring 2023. This is a very rough draft of an activity.**

Provides materials that can be used as in-class activities or onboarding tutorials for students or independent developers learning to work within [LibreFoodPantry](https://librefoodpantry.org).

The set of [POGIL](https://pogil.org) activities teach students to

- work with GitLab epics and issues
- use the the containerized develpment tools to make changes to a project
- follow LibreFoodPantry's adopted workflow to
  - claim an issue
  - create a feature branch and associated merge-request
  - provide correctly formatted commit messages 
    - following [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
    - with appropriate `Co-authored-by` attribution
    - `Closes` to close associated issues
  - squash commits on a feature branch into a single commit
  - request a review
  - review a merge request
  - merge a squashed commit into the main branch

The activities are developed around a version of the [Thea's Pantry GuestInfoSystem](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem). [Theas Pantry](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry) is a member project of LibreFoodPantry. These activities use the WorkflowKit - a GitLab group consisting of

- code repositories, each "captured" at a particular commit
- documentation and issue repositories, each "captured" at a particular commit
- this activities repository, "captured" at a particular commit
- an instructor guide

To use these activities, follow the instructions in the [WorkflowKit's README](https://gitlab.com/LibreFoodPantry/training/workflow/workflowkit/-/blob/main/README.md) to create one or more GitLab groups for you, your students, or your team(s) to use while working through the activities.

The working code given to explore, modify, and extend is containerized in the same way that LFP services are containerized in Docker.

---

&copy; 2023 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
