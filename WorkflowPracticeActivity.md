# Workflow Practice Activity

For this activity we will be using the issues created as part of the [Planning Practice Activity](https://gitlab.com/LibreFoodPantry/training/planning/planning-activities/-/blob/main/PlanningPracticeActivity.md?ref_type=heads).

## Do this as a team. Choose one person to display their screen while doing this:

1. Go to your team's sub-group.
2. Go to `Plan > Issue Board`.
3. Decide which team member will work on each issue in the `Spring Backlog`.

## Each team member will do these steps individually. Ask for help from your teammates if necessary

1. Install the [Gitpod Browser Extension](https://www.gitpod.io/docs/configure/user-settings/browser-extension)
2. Open the issue you have been assigned by your team, and assign it to yourself.
3. Click on the `G Open` button to open the appropriate project in Gitpod.
4. Notice that you are in a newly created branch for your issue.
5. Nothing will have changed in this branch yet, but it's good practice to do `git pull` to make sure there are no remote changes.
6. Make some changes to pretend to complete the issue.

    - Make at least two commits because we will need them in a future step where we squash the commits down to a single commit before merging into `main`.
    - It's a good idea to prefix your commit message with a Conventional Commit type (although we will have a chance to make sure this is correct in the squashed commit later.) You probably want to use `feature` or `fix` or `docs` for the issues in this practice.
    - Make sure you push your changes.
7. Follow the link to create your merge request that was shown in the `git push` message.
   1. Notice the branch name and which branch it will be merged into. Make sure that makes sense to you.
   2. Look at the title if the merge request. Check `Mark as draft`. Notice that `Draft:` has been added, and the note below the title.
   3. Read the description. Discuss with your team. Is there anything that you don't understand?
   4. Notice the last line of the description (after all the steps). What will that do? 
8. Click `Create merge request`

    - Look at the `commits` tab to see your commits.
    - Look at the `changes` tab to see the changes to the file(s).
    - Look at the `pipelines` tab to see if the pipelines all passed. You should see green check marks for all pipelines.
        - If you see a red X, you can check what stage and check failed and see what you need to do to fix it. Ask for help from your team or your instructor.
        - If you did not use a Conventional Commit prefix on your commit messages you will see `lint-commit-message` fail. We can fix that when we squash the commit next.
9.  Look at the `overview` tab.

    - Read the section on `Squashing`.
    - Follow the instructions to squash your commits and write a good commit message.
        - Make sure to use the appropriate type.
        - Make sure your commit message describes what was done by all the commits you are squashing. Write a single line with a high-level description for the first line. If you need give more details, you add additional lines below the first.
        - Practice adding a `Co-authored-by` line with the information for one of your teammates. We will use this in the future if more than one team member has pushed commits to the same branch so that everybody gets credit for their contribution.
        - Make sure the `Closes` line has your issue number.
10. One you have done the `git push --force origin $(git branch --show-current)` command, return to your merge request and wait for the pipelines to complete.
    - In the meantime check the `commits` tab and make sure your squashed commit looks fine - single commit and the correct commit message.
    - Once all the pipelines all complete successfully (green checks) move to the next step. If they don't try to fix the problems.
11. When the pipelines pass mark the merge request as ready for review. There is a button for this under the `overview` tab.
12. Assign someone on your team as a reviewer. Let them know in a comment that you would like them to review your merge request. In the meantime, review one of your teammate's merge request.
13. Once they have reviewed your merge request

    - fix anything they suggest fixing and ask for another review
    - if they approve, merge!
        - Make sure `Delete source branch` is checked.
        - Check `Edit commit message` and make sure everything looks fine. This is the commit message for the merge, and so won't have the messsage your wrote. **However, you will want to check the `Closes` line to make sure it includes only the issue you worked on.**
14. Click the `Merge` button.

    - Check `Merge Requests` to see that yours has merged.
    - Check `Issues` to see that your has closed.
    - Check `Repository>Branches` to see that your branch has been deleted.
15. You can delete your local branch:

    ```bash
    git switch main
    git branch -D <branch-name>
    ```

16. If your merge request should have updated the version number, check the `tags` from the main project page to see if a new version number tag was created. Also check `Packages and Registries>Container Registry` for a new Docker image.
17. Be sure to stop your Gitpod Workspace with the terminal command:
    ```bash
    gp stop
    ```

&copy; 2024 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA
